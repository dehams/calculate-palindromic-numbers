#calculate palindromic numbers

FirstNumber = int(input("Enter a number you want to start with: "))
Cycles = int(input("\nHow many cycles do you want to calculate? "))

def Umkehr(Number):
    """ inverts the given number """
    Reverse = 0
    while(Number > 0):
        Reminder = Number %10
        Reverse = (Reverse *10) + Reminder
        Number = Number //10
    return Reverse

PFound = 0
Count = 0

print("\n\nCalculating %s cycles, " %Cycles + "beginning with number %s" %FirstNumber)

while(Count < Cycles):
    esreveR = Umkehr(FirstNumber)
    Summe = int(FirstNumber) + int(esreveR)
    print("%d" %FirstNumber)
    if esreveR == FirstNumber:
        PFound = PFound + 1
        print("*%d" %esreveR + " is palindrom number %d" %PFound)
    else:
        print("%d" %esreveR)
    print("%d" %Summe)
    Count = Count + 1
    FirstNumber = Summe
    
print("\nFinished!\nAfter %d cycles" %Cycles + " we found %d palindromic numbers." %PFound)